/* 
    This file is just a wrapper for sdl, it also handles texture caching

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifndef __graphics_h__
#define __graphics_h__

#include "point.h"

void graphics_initialize();

void graphics_changemode();

int load_bmp(char *);

int load_bmps(char *, int, int);

int load_bmpstrip(char *, int);

void refresh_screen();

void display_bmp(unsigned int, unsigned int, unsigned int);

void display_bg();

void free_bmps();

void chop_bg(point, point);

void set_backimage(char *);

void set_bg(char *);

point bmp_size(unsigned int);

#endif
