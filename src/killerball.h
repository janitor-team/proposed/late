#ifndef __killerball_h__
#define __killerball_h__
#include "ball.h"

class kball : public ball_base
{
 public:
  kball(ball *);
  void display();
  void update(float);
  ball_base * duplicate(ball *);
  unsigned int getradius()
    {
      return 10;
    }
};

#endif
