/* 
    This file handles the functions that change depending on the operating
    system

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "arch.h"
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <unistd.h>

using namespace std;

bool fileexists(char * filename)
{
  ifstream temp(filename);
  if (temp.is_open())
    {
      temp.close();
      return 1;
    }
  return 0;
}

void absolutepath(char * filename, char * output)
{
  if (fileexists(filename))
    {
      output = strcpy(output, filename);
      return;
    }

  char n[200];
  snprintf(n, 200, "../%s", filename);
  if (fileexists(n))
    {
      output = strcpy(output, n);      
      return;
    }

  snprintf(n, 200, DATADIR"/%s", filename);
  if (fileexists(n))
    {
      output = strcpy(output, n);      
      return;
    }


}

void hiscoresfile(char * output)
{
  char n[200];
  snprintf(n, 200, LOCALSTATEDIR"/lib/games/late.scores");
  output = strcpy(output, n);
  return;
}


void conffile(char * output)
{
  char n[200];
  snprintf(n, 200, "%s/.laterc", getenv("HOME"));
  output = strcpy(output, n);
}

string username()
{
  char temp[200];
  return string(cuserid(temp));
}
