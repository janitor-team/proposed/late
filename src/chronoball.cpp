/* 
    This file handles those little teleporting balls

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "chronoball.h"
#include "graphics.h"
#include "game.h"
using namespace std;

static unsigned int chronoballbmp;
static bool chronoballbmpassigned;


chronoball::chronoball(ball * pack)
{
  package = pack;
  timer = 0;
}
void chronoball::display()
{
  if (chronoballbmpassigned == 0)
    {
      chronoballbmp = load_bmpstrip("chronoball.bmp",10);
      chronoballbmpassigned = 1;
    }

  int pic;
  pic = chronoballbmp + int(timer);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2); 
}

ball_base * chronoball::duplicate(ball * pack)
{
  chronoball * output = new chronoball(pack);
  output->timer = timer;
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  return output;
}

void chronoball::update(float amount)
{
  ball_base::update(amount);
  timer += amount * 0.02;
  if (timer >= 10)
    {
      random_place(package);
      timer = 0;
    }
}

