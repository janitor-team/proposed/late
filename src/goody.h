#ifndef __goody_h__
#define __goody_h__

#include "ball.h"

class lifeup : public ball_base
{
 float animate;
 public:
  lifeup(ball *);
  unsigned int getradius()
    {
      return 10;
    }
  void display();
  ball_base * duplicate(ball *);
  int hitchopper();
  int ishostile()
    {
      return 0;
    }
  void exploded();
  void update(float);
};

class lazershot : public ball_base
{
  float animate;
 public:
  lazershot(ball *);
  unsigned int getradius()
    {
      return 10;
    }
  void display();
  ball_base * duplicate(ball *);
  int hitchopper();
  int ishostile()
    {
      return 0;
    }
  void exploded();
  void update(float);
};

class magnetitem : public ball_base
{
  float animate;
 public:
  magnetitem(ball *);
  unsigned int getradius()
    {
      return 10;
    }
  void display();
  ball_base * duplicate(ball *);
  int hitchopper();
  int ishostile()
    {
      return 0;
    }
  void exploded();
  void update(float);
};

class bonus : public ball_base
{
  float animate;
 public:
  bonus(ball *);
  unsigned int getradius()
    {
      return 10;
    }
  void display();
  ball_base * duplicate(ball *);
  int hitchopper();
  int ishostile()
    {
      return 0;
    }
  void exploded();
  void update(float);
};

class brpu : public ball_base
{
  float animate;
 public:
  brpu(ball *);
  unsigned int getradius()
    {
      return 10;
    }
  void display();
  ball_base * duplicate(ball *);
  int hitchopper();
  int ishostile()
    {
      return 0;
    }
  void exploded();
  void update(float);
};


#endif
