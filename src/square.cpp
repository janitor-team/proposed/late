/* 
    This file handles the rectangular reigeons of the field of play

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "square.h"

//checks to see if a point is within its bounds

bool square::isinside(const point totest) const
{
  return totest.x <= int(rightx) && totest.x >= int(leftx) && 
    totest.y <= int(bottomy) && totest.y >= int(topy);
}

bool square::isinside(const unsigned int x, const unsigned int y) const
{
  return x <= rightx && x >= leftx && 
    y <= bottomy && y >= topy;
}

//returns two squares formed from this one
vector<square> square::chopx(const unsigned int leftmargin, 
			     const unsigned int rightmargin) const
{
 
  square temp;
  vector<square> output;

  temp.leftx = leftx;
  temp.rightx = leftmargin;
  temp.topy = topy;
  temp.bottomy = bottomy;

  if (leftx < leftmargin)
    output.push_back(temp);

  temp.leftx = rightmargin;
  temp.rightx = rightx;
  temp.topy = topy;
  temp.bottomy = bottomy;

  if (rightx > rightmargin)
    output.push_back(temp);

  return output;
}

//returns two squares formed from this one
vector<square> square::chopy(const unsigned int topmargin, 
			    const unsigned int bottommargin) const
{
  square temp;
  vector<square> output;

  temp.leftx = leftx;
  temp.rightx = rightx;
  temp.topy = topy;
  temp.bottomy = topmargin;

  if (topy < topmargin)
    output.push_back(temp);

  temp.leftx = leftx;
  temp.rightx = rightx;
  temp.topy = bottommargin;
  temp.bottomy = bottomy;

  if (bottomy > bottommargin)
    output.push_back(temp);


  return output;
}
