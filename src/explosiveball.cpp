#include "explosiveball.h"
#include "graphics.h"
#include "game.h"
#include <cmath>
using namespace std;

static unsigned int expballbmp;
static bool expballbmpassigned;

static unsigned int expbmp;
static bool expbmpassigned;

expball::expball(ball * pack)
{
  package = pack;
  animate = 0;
  fuze = 100000;
}
void expball::display()
{
  if (expballbmpassigned == 0)
    {
      expballbmp = load_bmpstrip("lightningstrip.bmp",9);
      expballbmpassigned = 1;
    }

  int pic;
  pic = expballbmp + int(animate);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2); 
}

ball_base * expball::duplicate(ball * pack)
{
  expball * output = new expball(pack);
  output->animate = animate;
  output->fuze = fuze;  
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  output->animate = animate;
  return output;
}

void expball::update(float amount)
{
  ball_base::update(amount);

  animate += 0.05 * amount;
  if (animate >= 9)
    animate = 0; 

  if (boundary->area() < 40000)
    blow_up(boundary);
}

explosion::explosion(point newc)
{
  center = newc;
  phase = 0;
}
void  explosion::display()
{
  if (expbmpassigned == 0)
    {
      expbmp = load_bmpstrip("blobstrip.bmp",10);
      expbmpassigned = 1;
    }

  int pic;
  pic = expbmp + int(phase);

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2); 
}
void  explosion::update(float amount)
{
  if (phase < 11)
    phase += amount / 5;
}
