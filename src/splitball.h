/* 
    This file handles those balls that devide when they smack into other ones

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#ifndef __splitball_h__
#define __splitball_h__

#include "ball.h"


class splitball : public ball_base
{
  int level;
 public:
  unsigned int getradius();
  float getmass();
  void specialcollide(ball *);
  void specialcollideb(ball *); 
  int react(ball *);  
  splitball(ball *);
  void display();
  void clone(int, float);
  ball_base * duplicate(ball *);
};

#endif
