#ifndef __highscores_h__
#define __highscores_h__
#include <string>
#include <vector>
using namespace std;

struct highscoreentry
{
  string name;
  int level;
  int score;
};

void add_highscore(highscoreentry);
vector<highscoreentry> gethighscores();
highscoreentry gethighscore();

#endif
