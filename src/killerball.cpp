#include "killerball.h"
#include "graphics.h"
#include "slicer.h"
#include "game.h"
#include <cmath>
using namespace std;

static unsigned int kballbmp;
static unsigned int activebmp;
static bool kballbmpassigned;

extern bool activechop;
extern slicer chopper;

kball::kball(ball * pack)
{
  package = pack;
}
void kball::display()
{
  if (kballbmpassigned == 0)
    {
      kballbmp = load_bmp("kball.bmp");
      activebmp = load_bmp("kballa.bmp");
      kballbmpassigned = 1;
    }
  int pic;
  if (activechop)
    pic = activebmp;
  else
    pic = kballbmp;

  display_bmp(pic, int(center.x) - bmp_size(pic).x / 2,
	      int (center.y) - bmp_size(pic).y / 2); 
}

ball_base * kball::duplicate(ball * pack)
{
  kball * output = new kball(pack);
  output->setcenter(center);
  output->setvelocity(velocity);
  output->setboundary(boundary);
  return output;
}

void kball::update(float amount)
{
  if (activechop)
    {
      floatpoint difference;
      difference = floatpoint(chopper.center) - center;
      velocity = difference / difference.magval() * velocity.magval();
      ball_base::update(amount); 
    }
  else
    ball_base::update(amount);

}
