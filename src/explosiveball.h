#ifndef __expball_h__
#define __expball_h__
#include "ball.h"

class expball : public ball_base
{
  float animate;
  float fuze;
 public:
  expball(ball *);
  void display();
  void update(float);
  ball_base * duplicate(ball *);
};

class explosion
{
 public:
  float phase;
  point center;
  explosion(point);
  void display();
  void update(float);
};

#endif
