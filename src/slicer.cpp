/* 
    This file controls those nasty chopping devices that hew up the landscape

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/


#include "slicer.h"
#include "graphics.h"

static unsigned int hpic;
static unsigned int vpic;
static unsigned int vtend;
static unsigned int vbend;
static unsigned int hlend;
static unsigned int hrend;

static unsigned int lhpic;
static unsigned int lvpic;
static unsigned int lvtend;
static unsigned int lvbend;
static unsigned int lhlend;
static unsigned int lhrend;

static bool inited;

slicer::slicer()
{
  done = 0;
  size1 = 0;
  size2 = 0;
  width = 10;
  cuttime = 0;
}

slicer::slicer(point c, float s, char d, int lazerish)
{
  done = 0;
  size1 = 0;
  width = 10;
  size2 = 0;
  speed = s;
  direction = d;
  center = c;
  lazerset = lazerish;
  slicer();
}

void slicer::update(float amount)
{

  size1 += speed * amount;
  size2 += speed * amount;

  if (direction == 'h' && lazerset == 0)
    {
      if (center.x - size1 < boundary->leftx)
	size1 = center.x - boundary->leftx;
      if (center.x + size2 > boundary->rightx)
	size2 = boundary->rightx - center.x;
      if (center.x - size1 == boundary->leftx && 
	  center.x + size2 == boundary->rightx)
	done = 1;
    }

  if (direction == 'v' && lazerset == 0)
    {
      if (center.y - size1 < boundary->topy)
	size1 = center.y - boundary->topy;
      if (center.y + size2 > boundary->bottomy)
	size2 = boundary->bottomy - center.y;
      if (center.y - size1 == boundary->topy && 
	  center.y + size2 == boundary->bottomy)
	done = 1;
    }
  if (direction == 'h' && lazerset == 1)
    {
      cuttime += amount;
      size1 = center.x - boundary->leftx;
      size2 = boundary->rightx - center.x;
      if (cuttime > 20)
	done = 1;
    }

  if (direction == 'v' && lazerset == 1)
    {
      cuttime += amount;
      size1 = center.y - boundary->topy;
      size2 = boundary->bottomy - center.y;
      if (cuttime > 20)
	done = 1;
    }

}

void slicer::display()
{
  if (done)
    return;

  //if the bitmaps for the chopper have not been loaded, load them
  if (!inited)
    {
      inited = 1;
      
      hpic = load_bmp("knifeh.bmp");
      vpic = load_bmp("knifev.bmp");
      vtend = load_bmp("knifevtt.bmp");
      vbend = load_bmp("knifevbt.bmp");
      hlend = load_bmp("knifehlt.bmp");
      hrend = load_bmp("knifehrt.bmp");
      lhpic = load_bmp("lazerh.bmp");
      lvpic = load_bmp("lazerv.bmp");
      lvtend = load_bmp("lazervtt.bmp");
      lvbend = load_bmp("lazervbt.bmp");
      lhlend = load_bmp("lazerhlt.bmp");
      lhrend = load_bmp("lazerhrt.bmp");
    }


  int isize = int(size1);
  int sections = isize / 10 - 1;

  for (int i = 0; i < sections; i++)
    {
      if (direction == 'h' && lazerset == 0)
	display_bmp(hpic, center.x - i * 10, center.y - width / 2);
      if (direction == 'v' && lazerset == 0)
	display_bmp(vpic, center.x - width / 2, center.y - i * 10);
      if (direction == 'h' && lazerset == 1)
	display_bmp(lhpic, center.x - i * 10, center.y - width / 2);
      if (direction == 'v' && lazerset == 1)
	display_bmp(lvpic, center.x - width / 2, center.y - i * 10);
    }

  isize = int(size2);
  sections = isize / 10 - 1;

  for (int i = 0; i < sections; i++)
    {
      if (direction == 'h' && lazerset == 0)
	display_bmp(hpic, center.x + i * 10, center.y - width / 2);
      if (direction == 'v' && lazerset == 0)
	display_bmp(vpic, center.x - width / 2, center.y + i * 10);
      if (direction == 'h' && lazerset == 1)
	display_bmp(lhpic, center.x + i * 10, center.y - width / 2);
      if (direction == 'v' && lazerset == 1)
	display_bmp(lvpic, center.x - width / 2, center.y + i * 10);
    }


  //draws the blade ends
  if (size2 + size1 > 40)
    {
      if (direction == 'h' && lazerset == 0)
	{
	  display_bmp(hlend, center.x - int(size1), center.y - width / 2);
	  display_bmp(hrend, center.x + int(size2) - 30, center.y - width / 2);
	}
      if (direction == 'v' && lazerset == 0)
	{
	  display_bmp(vtend, center.x - width / 2, center.y - int(size1));
	  display_bmp(vbend, center.x - width / 2, center.y + int(size2) - 30);
	}      
      if (direction == 'h' && lazerset == 1)
	{
	  display_bmp(lhlend, center.x - int(size1), center.y - width / 2);
	  display_bmp(lhrend, center.x + int(size2) - 30, center.y - width / 2);
	}
      if (direction == 'v' && lazerset == 1)
	{
	  display_bmp(lvtend, center.x - width / 2, center.y - int(size1));
	  display_bmp(lvbend, center.x - width / 2, center.y + int(size2) - 30);
	}      
    }
}
