#ifndef __player_h__
#define __player_h__

#include "point.h"

class player
{
  point center;
  char direction;
 public:
  player();
  point getcenter();
  void setcenter(point);
  void display();
  char getdirection()
    {
      return direction;
    }
  void setdirection(char to)
    {
      direction = to;
    }
};

#endif
