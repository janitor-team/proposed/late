/* 
    This is the little thing on the side that tells one how many lives one
    has and other useful information

    Copyright, 2003 Caleb Moore
    
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include "graphics.h"
#include "text.h"
#include "highscores.h"
#include <sstream>
#include <stdio.h>

using namespace std;

static unsigned int font;
static unsigned int background;
static bool assigned;

extern int clearnessneeded;
extern int clearness;
extern int currentlevel;
extern int lives;
extern int score;
extern int lazershots;
extern int magnetshots;

void display_sidebar()
{
  if (assigned == 0)
    {
      assigned = 1;
      background = load_bmp("sidebar.bmp");
      font = load_font("font2.bmp");
    }

  display_bmp(background, 700, 0);
  
  char n [200];
  snprintf(n, 200, "%i", currentlevel);
  print_text(n, font, 720, 35);
  snprintf(n, 200, "%i", lives);
  print_text(n, font, 750, 75);
  snprintf(n, 200, "%i", clearness);
  print_text(n, font, 720, 115);
  snprintf(n, 200, "%i", clearnessneeded);
  print_text(n, font, 760, 115);
  snprintf(n, 200, "%i", score);
  print_text(n, font, 750, 155);
  print_text(gethighscore().name, font, 720, 185);
  snprintf(n, 200, "%i", gethighscore().score);
  print_text(n, font, 740, 205);

  snprintf(n, 200, "%i", lazershots);
  print_text(n, font, 750, 255);
  snprintf(n, 200, "%i", magnetshots);
  print_text(n, font, 750, 295);
}
